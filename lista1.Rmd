---
title: "lista1"
output: html_document
---

#### Questão 1
###### Gere um banco de dados com 50 itens e 1000 respondentes a partir do modelo logístico de 3 parâmetros:

```{r cars}
###### from exemplo de Prof. Antonio Eduardo

set.seed(123) # semente (alterado parametro)

no.itens <- 50 # numero de itens
no.resp <- 1000 # numero de respondentes

aa <- rlnorm(no.itens,0,1) # vetor de parametros de discriminacao
bb <- rnorm(no.itens,0,1) # vetor de parametros de dificuldade
cc <- rbeta(no.itens,40,480) # vetor de parametros de acerto ao acaso (alterado parametro)

# proficiencias reais

theta <- rnorm(no.resp,0,1) # proficiencias reais

# calculo da matriz de probabilidades de acerto para geração dos dados

mat.prob <- matrix(0,no.resp,no.itens)

for (i in 1:no.itens) mat.prob[,i] <- cc[i]+(1-cc[i])/(1+exp(-aa[i]*(theta-bb[i])))

# geracao dos dados

n.aux <- no.resp*no.itens

dados.sim <- matrix(sign(c(mat.prob)-runif(n.aux,0,1)),nrow=no.resp)
dados.sim[dados.sim==-1] <- 0
```

###### A) Calcule o coeficiente de correlação ponto-bisserial para cada um dos itens:

```{r}
# correlacao ponto bisserial

escores <- apply(dados.sim,1,sum) # escores individuais
prop <- apply(dados.sim,2,sum)/no.resp # proporcao de acertos de cada item

cor.pb <- rep(0,no.itens) # correlacao ponto bisserial
for (i in 1:no.itens) cor.pb[i] <- cor(dados.sim[,i],escores)

print(data.frame(cor.pb))
```

###### B) Plote os valores do coeficiente obtidos em "A":
```{r, echo=FALSE}
plot(cor.pb)
```

###### C) Elabore um diagrama de dispersão do coeficiente de correlação ponto-bisserial e o parâmetro de discriminação dos itens:
```{r, echo=FALSE}
plot(aa,cor.pb)
```

###### D) Trace a curva de regressão não paramétrica de Nadaraya-Watson:
```{r, echo=FALSE}
plot(aa,cor.pb) # mostra a correlacao entre o parametro de dificuldade do item 
# e o valor da correlacao ponto bisserial

lines(ksmooth(aa, cor.pb, kernel = c("normal"), bandwidth = 0.5, range.x = range(aa),
              n.points = max(100L, length(aa))))
```

###### E) Calcule a correlação linear entre o coeficiente de correlação ponto-bisserial e o parâmetro de discriminação:
```{r}
## correlacao entre o parametro de discriminacao e o coeficiente de correlacao ponto bisserial
cor(aa,cor.pb)
```

###### F) O que o diagrama de dispersão e o valor da correlação linear parecem indicar:

O diagrama de dispersão parece indicar uma relação linear crescente entre o parâmetro "a" com a correlação ponto-bisserial para valores de "a" até 4. Já o valor do coeficiente de correlação linear revela uma correlação positiva de moderada a forte.

###### G) Elabore um diagrama de dispersão do coeficiente de correlação ponto-bisserial e o parâmetro de dificuldade dos itens:
```{r, echo=FALSE}
plot(bb,cor.pb) # mostra a correlacao entre o parametro de dificuldade do item 
# e o valor da correlacao ponto bisserial
```

###### H) Trace a curva de regressão não paramétrica de Nadaraya-Watson:
```{r, echo=FALSE}
plot(bb,cor.pb) # mostra a correlacao entre o parametro de dificuldade do item 
# e o valor da correlacao ponto bisserial

lines(ksmooth(bb, cor.pb, kernel = c("normal"), bandwidth = 0.5, range.x = range(bb),
              n.points = max(100L, length(bb))))
```

###### I) Calcule a correlação linear entre o coeficiente de correlação ponto-bisserial e o parâmetro de dificuldade dos itens:
```{r}
## correlacao entre o parametro de dificuldade do item e o coeficiente de correlacao ponto bisserial
cor(bb,cor.pb)
```

###### J) O que o diagrama de dispersão e o valor da correlação linear parecem indicar:

O diagrama de dispersão parece indicar uma ausência de relação linear entre o parâmetro "b" com a correlação ponto-bisserial para valores sendo que os maiores valores de correlação estiveram concentrados entre valores de b de -1 a 1 mas de maneira dispersa. Já o valor do coeficiente de correlação indica uma correlação negativa e fraca entre o parâmetro de dificuldade do item e o coeficiente de correlação ponto-bisserial.

###### K) Calcule o coeficiente de correlação bisserial para cada item:
```{r}
# correlacao bisserial

cor.b <- cor.pb*sqrt(prop*(1-prop))/dnorm(qnorm(prop,0,1),0,1)

print(data.frame(cor.pb))
```

###### L) Elabore um diagrama de dispersão do coeficiente de correlação ponto-bisserial e do coeficiente de correlação bisserial:
```{r, echo=FALSE}
plot(cor.pb,cor.b)
```

###### M) Calcule a correlação linear entre os dois coeficientes:
```{r}
## correlacao entre o coeficiente de correlacao bisserial e o coeficiente de correlacao ponto bisserial
cor(cor.b,cor.pb)
```

###### N) O que o diagrama de dispersão e o valor da correlação linear parecem indicar:

O diagrama de dispersão revela uma relação linear crescente e forte entre os dois coeficientes de correlação, visto que quase todas as observações se encontraram sobre a reta diagonal do gráfico. O valor da correlação linear confirmou o exposto pelo diagrama sendo forte e positivo.

#### Questão 2

A função fii retorna uma tabela com os resultados calculados para o item pela função de informação nos modelos logísticos de 1, 2 e 3 parâmetros dado os parâmetros a,b,c e theta:

```{r}
library(Deriv)

fii <- function(a,b,c,theta){

  P3 <- function(theta) c+(1-c)/(1+exp(-a*(theta-b)))
  P2 <- function(theta) 1/(1+exp(-a*(theta-b)))
  P1 <- function(theta) 1/(1+exp(-(theta-b)))
  
  Pi3 <- P3(theta)
  Pi2 <- P2(theta)
  Pi1 <- P1(theta)
  
  Qi3 <- 1 - Pi3
  Qi2 <- 1 - Pi2
  Qi1 <- 1 - Pi1
  
  Df3 <- Deriv(P3)
  Df2 <- Deriv(P2)
  Df1 <- Deriv(P1)
  
  FII3 <- Df3(theta)^2/(Pi3*Qi3)
  FII2 <- Df2(theta)^2/(Pi2*Qi2)
  FII1 <- Df1(theta)^2/(Pi1*Qi1)
  
  tab <- data.frame(FII1, FII2, FII3)
  
  return(tab)
}

```

Exemplo utilizando as 20 primeiras observações do vetor theta, utilizando a = 0.8, b = 0.5 e c = 0.3: 

```{R}
fii(a = 0.8, b = 0.5, c = 0.3,theta[1:20])
```
A função abaixo gera um gráfico da FII do item para um dado vetor de parâmetros (a, b, c) para um item específico:

```{R}
fii.grafico <- function(a,b,c, theta = c(-4.5,-4,-3.5,-3,-2.5,-2,-1.5,-1,-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4)){

  P3 <- function(theta) c+(1-c)/(1+exp(-a*(theta-b)))
  P2 <- function(theta) 1/(1+exp(-a*(theta-b)))
  P1 <- function(theta) 1/(1+exp(-(theta-b)))
  
  Pi3 <- P3(theta)
  Pi2 <- P2(theta)
  Pi1 <- P1(theta)
  
  Qi3 <- 1 - Pi3
  Qi2 <- 1 - Pi2
  Qi1 <- 1 - Pi1
  
  Df3 <- Deriv(P3)
  Df2 <- Deriv(P2)
  Df1 <- Deriv(P1)
  
  FII3 <- Df3(theta)^2/(Pi3*Qi3)
  FII2 <- Df2(theta)^2/(Pi2*Qi2)
  FII1 <- Df1(theta)^2/(Pi1*Qi1)
  
  tab <- data.frame(FII1, FII2, FII3, theta) 
  
  par(mfrow=c(2,2))
  plot(tab[,4], tab[,1], main = "1 Parâmetro")
  lines(tab[,4], tab[,1], type = "l", lty = 1)
  plot(tab[,4], tab[,2], main = "2 Parâmetros")
  lines(tab[,4], tab[,2], type = "l", lty = 1)
  plot(tab[,4], tab[,3], main = "3 Parâmetros")
  lines(tab[,4], tab[,3], type = "l", lty = 1)
}

```

Exemplo para a = 0.8, b = 0.5 e c = 0.3:

```{r}
fii.grafico(a = 0.8, b = 0.5, c = 0.3)
```

#### Questão 3

###### A) Para cada item, calcule a probabilidade de acerto nas proficiências -3, -2, -1, 0, 1, 2 e 3:
```{r}

calc <- matrix(0,7,6)

aa <- c(1.8,0.7,1.8,1.2,1.2,0.5)
bb <- c(1,1,1,-0.5,0.5,0)
cc <- c(0,0,0.25,0.2,0,0.1)
theta <- c(-3,-2,-1,0,1,2,3)

for (i in 1:6) calc[,i] <- cc[i]+(1-cc[i])/(1+exp(-aa[i]*(theta-bb[i])))

colnames(calc) <- c("item 1","item 2","item 3","item 4","item 5","item 6")
rownames(calc) <- c(-3,-2,-1,0,1,2,3)
calc <- as.data.frame(calc)

calc$proficiencia <- c(-3,-2,-1,0,1,2,3)

print(calc[,-7])

```

###### B) Construa a curva característica de cada item (CCI):

```{r}
  par(mfrow=c(2,3))
  for(i in 1:6){
      plot(calc[,7], calc[,i], main = paste0("Item ",i))
      lines(calc[,7], calc[,i], type = "l", lty = 1)
  }
```

###### C) Qual o item mais fácil?

O item que globalmente foi mais fácil, foi o item 4 com o menor valor do parâmetro b.

###### D) Qual o item que menos discrimina?

O item 6 em virtude de ter globalmente o menor valor do parâmetro a.

###### E) Qual o item no qual um aluno com proficiência igual a 0 tem a maior probabilidade de acerto?

O item 4.

###### F) Qual o item é mais fácil para a proficiência igual a -1? E ára a proficiência 0?

O item 4 é o mais fácil tanto em proficiência = -1 quanto em proficiência = 0.

###### G) Existem dois itens que são igualmente difíceis na proficiência -1? E na proficiência 1?

Para proficiência igual a -1 não existe itens igualmente difíceis. Já para proficiência igual a 1 temos os itens 1 e 2 com probabilidades iguais de acerto.
